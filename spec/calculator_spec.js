var calculate = require("..");

describe("Calculator Application", () => {
        it("it accepts the symbol, stake, and risk tolerance (0=100% or undefined for unlimited risk), and determines the buy or sell rating, price, stop and limit.", (done) => {
            calculate("MSFT", "10,000", "20%", (trade) => {
              expect(["BUY" ,"SELL"]).toContain(trade.type);
              console.info(trade);
              done();
            });
        });
});

describe("A suite is just a function", function() {
  var a;

  it("and so is a spec", function() {
    a = true;

    expect(a).toBe(true);
  });
});